# LtTimer
- Just JavaFX8 GUI Application.
- There are some issues in this app. FIXME!!!🙏
    - Sometimes sounds are not played.
    - Not auto layout UI.
    - No build script.
    - Not OOP code.

# Required
- java8
- sound files with the following names(These files will be loaded [here](https://bitbucket.org/serihiro/lt-timer/src/234bbc056c20129a74b3c2cd696852714901a51e/src/application/Main.java?at=master&fileviewer=file-view-default#Main.java-30))
    - `start.wav`
    - `remaining10m.wav`
    - `timeup`

# Hot to start develop
  1. Clone this repository.
  2. Import as `JavaFX Project` into Eclipse.
